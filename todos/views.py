from django.shortcuts import redirect
from django.views.generic.list import ListView
from todos.models import TodoList
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView
from django.urls import reverse_lazy


class TodoListListView(ListView):
    model = TodoList
    template_name = "todos/list.html"


class TodoListDetailView(DetailView):
    model = TodoList
    template_name = "todos/detail.html"


class TodoListCreateView(CreateView):
    model = TodoList
    template_name = "todos/create.html"
    fields = ["name"]

    def form_valid(self, form):
        plan = form.save(commit=False)
        plan.owner = self.request.user
        plan.save()
        form.save_m2m()
        return redirect("todo_list_detail", pk=plan.id)

    def get_success_url(self):
        return reverse_lazy("todo_list_list")


# Create your views here.
